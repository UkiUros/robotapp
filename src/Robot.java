import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Robot {


    public static void main(String[] args) {
        String fileName = "src/robot.03.in.txt";
        System.out.println("Reading from file...");
        System.out.println("Content: " + readFromFile(fileName));
    }


    private static String readFromFile(String fileName) {
        try {
            File myObj = new File(fileName);
            Scanner myReader = new Scanner(myObj);
            StringBuilder stringBuilder = new StringBuilder();
            while (myReader.hasNextLine()) {
                stringBuilder.append(myReader.nextLine()).append("\n");
            }
            myReader.close();
            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return "";
        }
    }
}
